import React from 'react';



function Math1(props) {
  
  const calc = (option) => {

    if(option==="square"){

      var result = Math.pow(props.a,2)+Math.pow(props.b,2)+(2*props.a*props.b);

      return (
        <p>The square of ({props.a}+{props.b}) is :<b> {result} </b></p>
      )
    }

    else if(option === "quadratic"){

      var root1 = (-props.b+Math.sqrt(Math.pow(props.b,2)-(4*props.a*props.c)))/(2*props.a);
      var root2 = (-props.b-Math.sqrt(Math.pow(props.b,2)-(4*props.a*props.c)))/(2*props.a);

      return (
        <p>The roots of quadratic equation <b> ({props.a}x^2+{props.b}x+{props.c})</b> is :<b> {root1} </b>and <b>{root2}</b></p>
      );
    }

    else if(option === "factorial"){

      var a = 1;
      for(var i =1;i<=props.a;i++){
         a = i*a;
      }

      return (
        <p>The factorial of <b> {props.a}</b> is :<b> {a}</b></p>
      );
    }

    else if(option === "root"){
      
      return (
        <p>The <b>{props.a}th</b> root of <b> {props.b}</b> is :<b> {Math.pow(props.b,1/props.a)}</b></p>
      );
    }

    else if(option === "trigno"){

      return (
        <div>
    <p><b>sin {props.a}</b> = <b> {Math.sin(props.a)}</b></p>
    <p><b>cos {props.a}</b> = <b> {Math.cos(props.a)}</b></p>
    <p><b>tan {props.a}</b> = <b> {Math.tan(props.a)}</b></p>
    </div>
      )
    }

    else{

      return(
        <div>
          <p>Please select a valid option.</p>
        </div>
      )
    }
  }

  return (
    <div>
      {
        calc(props.option) 
      }
    </div>
  );
}

export default Math1;
