import React , { useState } from 'react';

import './App.css';
import Math1 from './Math1.js'

function App() {

  const [option,change_option] = useState();
  const [a,change_a] = useState('[Input a]');
  const [b,change_b] = useState('[Input b]');
  const [c,change_c] = useState('[Input c]');


  const select_option=(e) => {
    change_option(e.target.value);
    change_a('[Input a]');
    change_b('[Input b]');
    change_c('[Input c]');
  }

  return (
    <div>
      <select onChange={select_option.bind(this)} name="calc" id="calc">
      <option value="select-opt">....Select an option....</option>
      <option value="square">(a+b) Squared</option>
      <option value="quadratic">Quadratic Equation </option>
      <option value="factorial">Factorial of a number</option>
      <option value="root">Nth root of a number</option>
      <option value="trigno">Trignometry (sin cos tan)</option>
      </select>



      {option==="square"  && 
        <div>
          <h4>Enter value of <b>a</b> and <b>b</b></h4>
          <label >a : </label>
          <input type="number" id="a" name="a" onChange={(e)=>change_a(e.target.value)} ></input>
          <label >b : </label>
          <input type="number" id="b" name="b" onChange={(e)=>change_b(e.target.value)} ></input>
          <Math1 option={option} a={a} b={b} ></Math1>
        </div>
      }


      {option==="quadratic"  && 
        <div>
          <h4>Enter value of <b>a</b> ,<b>b</b> and <b>c</b></h4>
          <label >a : </label>
          <input type="number" id="a" name="a" onChange={(e)=>change_a(e.target.value)} ></input>
          <label >b : </label>
          <input type="number" id="b" name="b" onChange={(e)=>change_b(e.target.value)} ></input>
          <label >c : </label>
          <input type="number" id="c" name="c" onChange={(e)=>change_c(e.target.value)} ></input>
          <Math1 option={option} a={a} b={b} c={c}></Math1>
        </div>
      }

      {option==="factorial"  && 
        <div>
          <h4>Enter value of <b>a</b> </h4>
          <label >a : </label>
          <input type="number" id="a" name="a" onChange={(e)=>change_a(e.target.value)} ></input>
          <Math1 option={option} a={a}  ></Math1>
        </div>
      }


      {option==="root"  && 
        <div>
          <h4>Enter value of <b>Root a</b> for <b>Number b</b></h4>
          <label >a : </label>
          <input type="number" id="a" name="a" onChange={(e)=>change_a(e.target.value)} ></input>
          <label >b : </label>
          <input type="number" id="b" name="b" onChange={(e)=>change_b(e.target.value)} ></input>
          <Math1 option={option} a={a} b={b} ></Math1>
        </div>
      }


      {option==="trigno"  && 
        <div>
          <h4>Enter value of <b>Degree a</b> </h4>
          <label >a : </label>
          <input type="number" id="a" name="a" onChange={(e)=>change_a(e.target.value)} ></input>
          <Math1 option={option} a={a}  ></Math1>
        </div>
      }
   
  
      {option==="select-opt"  && 
        <Math1 option={option}></Math1>
      }

    </div>
  );
}

export default App;
