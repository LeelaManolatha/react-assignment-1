**React Assignment 1**

***App.js*** takes input from the user based on the selected option from the dropdown menu of mathematical operations.

***Math1.js*** returns a JSX element to display the result of the selected Mathematical operation.

*Note* : Math1.js has not been named Math.js since it was overwriting the Math() function of javascript.